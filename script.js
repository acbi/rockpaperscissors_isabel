// Author: Isabel Griffith
// Date: 24.02.2024
// Description: TODO

// CLASSES
class Shape {

    /**
     * Represents a Shape object.
     * @constructor
     * @param {string} shapeName - The name of the  Shape
     * @param {string[]} arrayShapePlayerImages - This array contains two image filenames representing images for a particular shape. One image for player 1 and the other for player 2.
     */
    constructor(parShapeName, parArrayImages) {
        //parArrayImages is an array of two image filenames.
        if (this.constructor == Shape) {
            throw new Error("Abstract classes can't be instantiated.");
        }
        else {
            this.shapeName = parShapeName;
            this.arrayShapePlayerImages = parArrayImages;
        }
        // 
    }



    /**
     * TODO
     * @method
     * @param {Shape} parSelectedShape - 
     * @returns {boolean} 
     */
    defeats(parShape) {
        throw new Error("Method defeats() must be implemented.");
    }
}
class Paper extends Shape {

    /**
 * Represents a custom shape constructor.
 * @constructor
 * @param {string} parShapeName - The name of the shape.
 * @param {Array} parArrayImages - An array of images associated with the shape.
 */
    constructor(parShapeName, parArrayImages) {
        super(parShapeName, parArrayImages);
    }

    /**
     * Determines if this shape defeats the given opponent shape.
     * @method
     * @param {Object} parShape - The opponent shape object to compare against.
     * @returns {boolean} Returns true if this shape defeats the opponent shape, otherwise false.
     */
    defeats(parShape) {
        if (parShape instanceof Rock) {
            return true;
        } else {
            return false;
        }
    }
}
class Rock extends Shape {

    /**
     * Represents a Rock, which is a Shape.
     * @class
     * @extends Shape
     */
    //rock = new Rock(‘Rock’, [‘images/player1Rock.png’,’images/player2Rock.png’]);
    constructor(parShapeName, parArrayImages) {
        super(parShapeName, parArrayImages);
        //...
    }

    /**
     * TODO
     * @method
     * @param {Shape} parSelectedShape - 
     * @returns {boolean} 
     */
    defeats(parShape) {
        if (parShape instanceof Scissors) {
            return true;
        } else {
            return false;
        }
    }
}
class Scissors extends Shape {

    /**
     * Represents a Rock, which is a Shape.
     * @class
     * @extends Shape
     */

    //paper = new Paper(‘Paper, [‘images/player1Paper.png’,’images/player2Paper.png’]);
    constructor(parShapeName, parArrayImages) {
        super(parShapeName, parArrayImages);
        //...
    }

    /**
     * TODO
     * @method
     * @param {Shape} parSelectedShape - 
     * @returns {boolean} 
     */
    defeats(parShape) {
        if (parShape instanceof Paper) {
            return true;
        } else {
            return false;
        }
    }
}
class Turn {
    //TODO DOKU

    constructor(player) {
        this.playerTurn = player;
        this.selectedShape = null; // Initially no shape selected
    }

    prepareTurn(parCurrentPlayer, parOtherPlayer, parIsPlayer1Turn, parRock, parPaper, parScissors) {
        var root = document.getElementById("game-container")
        // Prepare 'look away' message if there are two players

        var lookAwayBox = document.createElement('div');
        lookAwayBox.classList.add('lookaway');

        var lookAway = document.createElement('p');

        var yourTurn = document.createElement('p');
        if (parIsPlayer1Turn) {
            lookAway.textContent = parOtherPlayer.playerName + ", look away!";
            yourTurn.textContent = parCurrentPlayer.playerName + "`s turn";
        }
        else {
            lookAway.textContent = parCurrentPlayer.playerName + ", look away!";
            yourTurn.textContent = parOtherPlayer.playerName + "`s Turn";
        }

        var selectShape = document.createElement('p');
        selectShape.textContent = "Select a shape:"

        lookAwayBox.appendChild(lookAway);
        root.appendChild(lookAwayBox);
        root.appendChild(yourTurn);
        root.appendChild(selectShape);

        var handbox = document.createElement('div');
        handbox.classList.add('handbox');

        var img1 = document.createElement('img');
        img1.src = rpsGame.rock.arrayShapePlayerImages[0];
        img1.alt = 'Placeholder Image';
        var img2 = document.createElement('img');
        img2.src = rpsGame.paper.arrayShapePlayerImages[0];
        img2.alt = 'Placeholder Image';
        var img3 = document.createElement('img');
        img3.src = rpsGame.scissors.arrayShapePlayerImages[0];
        img3.alt = 'Placeholder Image';

        var currentTurn = this;
        img1.addEventListener('click', function () {
            currentTurn.selectedShape = parRock;
            rpsGame.playRound(rpsGame.currentRoundNumber);

        });
        img2.addEventListener('click', function () {
            currentTurn.selectedShape = parPaper;
            rpsGame.playRound(rpsGame.currentRoundNumber);
        });
        img3.addEventListener('click', function () {
            currentTurn.selectedShape = parScissors;
            rpsGame.playRound(rpsGame.currentRoundNumber);
        });

        handbox.appendChild(img1);
        handbox.appendChild(img2);
        handbox.appendChild(img3);
        root.appendChild(handbox);
    }
}
class Round {

    //TODO DOKU
    constructor(parArrayTurns) {
        this.currentTurnNumber = 0;
        this.arrayTurns = parArrayTurns;
        this.roundWinner = null; //TODO PLAYER
    }

    //TODO DOKU
    getRoundWinner() {
        return this.roundWinner.playerName;
    }
}
class Player {
    /**
     * Represents a Person object.
     * @constructor
     * @param {string} playerName - The name of the person
     * @param {number} score - The score of the person
     */
    constructor(playerName) {
        this.playerName = playerName;
        this.score = 0;
    }

    /** 
     * Greets the person. TODO
     * @method
     * @param {Shape} parSelectedShape is of type Shape. This represents the shape selected by the player.
     * @param {RPS_Game} parRPSGame is of type RPS_GAME. This represents the current instance of the RPS_GAME class.
        This is needed to represent the rock, paper, and scissors of the current RPS_GAME.

        parSelectedShape: Shape, parRPSGame: RPS_GAME
     * @returns {Shape} This method returns the shape selected by the player.
     */
    selectShape(parSelectedShape, parRPSGame) {
        //TODO GUI
        return true;
    }

}
class RPS_Game {

    /**
     * Represents a Person object.
     * @constructor
     * @param {Player} player1 - Player 1 object of RPS_GAME instance.
     * @param {Player} player2 - Player 2 object of RPS_GAME instance.
     */

    constructor(player1, player2, numberOfRounds) {
        this.player1 = player1;
        this.player2 = player2;
        this.arrayRounds = new Array();
        for (let i = 0; i < numberOfRounds; i++) {
            let round = new Round([new Turn(this.player1), new Turn(this.player2)])
            this.arrayRounds.push(round);
        }
        this.currentRoundNumber = 0;
        this.rock = new Rock('Rock', ['images/player1Rock.png', 'images/player2Rock.png']);
        this.paper = new Paper('Paper', ['images/player1Paper.png', 'images/player2Paper.png']);
        this.scissors = new Scissors('Scissors', ['images/player1Scissors.png', 'images/player2Scissors.png']);
    }

    // Methods
    isGameOver() {
        let minWinNumber = null;
        // Check if number is straight or odd
        if (this.arrayRounds.length % 2 == 0) {
            //straight 
            minWinNumber = (this.arrayRounds.length / 2) + 1;
        } else {
            //odd
            minWinNumber = Math.round(this.arrayRounds.length / 2)
        }

        if (this.currentRoundNumber == this.arrayRounds.length) {
            return true;
        }
        else if (this.player1.score >= minWinNumber || this.player2.score >= minWinNumber) {
            return true;
        }
        else {
            return false;
        }
    }

    //TODO DOKU
    getGameWinner() {
        if (this.player1.score == this.player2.score) {
            return "Draw!"
        }
        else if (this.player1.score > this.player2.score) {
            return this.player1.playerName;
        }
        else {
            return this.player2.playerName;
        }
    }

    //TODO DOKU
    playRound(parCurrentRoundNumber) {
        var root = document.getElementById("game-container")
        var header = document.createElement('h2');
        header.textContent = "Round " + (parCurrentRoundNumber + 1) + " of " + this.arrayRounds.length;

        var paragraph = document.createElement('p');
        paragraph.textContent = 'SCORE';

        var scorePlayerOne = document.createElement('p');
        scorePlayerOne.textContent = this.player1.playerName + ": " + this.player1.score;

        var scorePlayerTwo = document.createElement('p');
        scorePlayerTwo.textContent = this.player2.playerName + ": " + this.player2.score;

        var scoreBox = document.createElement('div');
        scoreBox.appendChild(scorePlayerOne);
        scoreBox.appendChild(scorePlayerTwo);
        scoreBox.classList.add('score-box');

        // Append child elements to the div
        root.innerHTML = "";
        root.appendChild(header);
        root.appendChild(paragraph);
        root.appendChild(scoreBox);

        let currentRound = this.arrayRounds[this.currentRoundNumber];


        if (currentRound.currentTurnNumber === 0) {
            let turnp1 = currentRound.arrayTurns[0];
            turnp1.prepareTurn(this.player1, this.player2, true, this.rock, this.paper, this.scissors)
            currentRound.currentTurnNumber++;
        }
        else if (currentRound.currentTurnNumber === 1) {
            let turnp2 = currentRound.arrayTurns[1];
            if (this.player2.playerName === "Computer") {
                let randomNumber = Math.floor(Math.random() * 3) + 1;

                switch (randomNumber) {
                    case 1:
                        turnp2.selectedShape = this.rock;
                        // Code to execute when random number is 1
                        break;
                    case 2:
                        turnp2.selectedShape = this.paper;
                        // Code to execute when random number is 2
                        break;
                    case 3:
                        turnp2.selectedShape = this.scissors;
                        // Code to execute when random number is 3
                        break;
                }
                currentRound.currentTurnNumber++;
                rpsGame.playRound(rpsGame.currentRoundNumber);
            }
            else {
                turnp2.prepareTurn(this.player1, this.player2, false, this.rock, this.paper, this.scissors)
            }
            currentRound.currentTurnNumber++;

        }
        else if (currentRound.currentTurnNumber === 2) {
            let turnp1 = currentRound.arrayTurns[0];
            let turnp2 = currentRound.arrayTurns[1];
            if (turnp1.selectedShape.defeats(turnp2.selectedShape)) {
                currentRound.roundWinner = turnp1.playerTurn;
                turnp1.playerTurn.score++;
            }
            else if (turnp2.selectedShape.defeats(turnp1.selectedShape)) {
                currentRound.roundWinner = turnp2.playerTurn;
                turnp2.playerTurn.score++;
            }
            else {

            }

            scorePlayerOne = document.createElement('p');
            scorePlayerOne.textContent = this.player1.playerName + ": " + this.player1.score;

            scorePlayerTwo = document.createElement('p');
            scorePlayerTwo.textContent = this.player2.playerName + ": " + this.player2.score;

            var winnerRound1 = document.createElement('p');
            winnerRound1.textContent = "Winner of this round";

            var winnerRound2 = document.createElement('p');
            if (currentRound.roundWinner === null) {
                winnerRound2.textContent = "DRAW!";
            }
            else {
                winnerRound2.textContent = currentRound.roundWinner.playerName;
            }

            var p1Label = document.createElement('p');
            p1Label.textContent = this.player1.playerName + " selected:";
            var img1 = document.createElement('img');
            img1.src = turnp1.selectedShape.arrayShapePlayerImages[0];
            img1.alt = 'Placeholder Image';
            var p1Shape = document.createElement('p');
            p1Shape.textContent = turnp1.selectedShape.shapeName;


            var p2Label = document.createElement('p');
            p2Label.textContent = this.player2.playerName + " selected:";
            var img2 = document.createElement('img');
            img2.src = turnp2.selectedShape.arrayShapePlayerImages[1];
            img2.alt = 'Placeholder Image';
            var p2Shape = document.createElement('p');
            p2Shape.textContent = turnp2.selectedShape.shapeName;


            var handboxbox = document.createElement('div');
            handboxbox.classList.add('handboxbox');

            root.appendChild(p1Label);
            root.appendChild(img1);
            root.appendChild(p1Shape);

            var handbox2 = document.createElement('div');
            handbox2.appendChild(p1Label);
            handbox2.appendChild(img1);
            handbox2.appendChild(p1Shape);
            handbox2.classList.add('handboxresult');

            var handbox3 = document.createElement('div');
            handbox3.appendChild(p2Label);
            handbox3.appendChild(img2);
            handbox3.appendChild(p2Shape);
            handbox3.classList.add('handboxresult');

            handboxbox.appendChild(handbox2);
            handboxbox.appendChild(handbox3);

            
            var buttonNextRound = document.createElement('button');
            buttonNextRound.textContent = "PLAY NEXT ROUND";
            buttonNextRound.id = 'buttonNextRound';
            buttonNextRound.classList.add('button');
            buttonNextRound.addEventListener('click', () => {
                const playNextRound = () => {
                    rpsGame.playRound(rpsGame.currentRoundNumber);
                };
                playNextRound();
            });

            root.innerHTML = "";
            root.appendChild(header);
            root.appendChild(paragraph);

            var scoreBox = document.createElement('div');
            scoreBox.appendChild(scorePlayerOne);
            scoreBox.appendChild(scorePlayerTwo);
            scoreBox.classList.add('score-box');
            root.appendChild(scoreBox);

            root.appendChild(header);
            root.appendChild(paragraph);
            root.appendChild(scoreBox);



            root.appendChild(winnerRound1);
            root.appendChild(winnerRound2);
            root.appendChild(handboxbox);
            root.appendChild(buttonNextRound);

            currentRound.currentTurnNumber++;
        }
        else {
            rpsGame.currentRoundNumber++;
            if (rpsGame.isGameOver()) {

                var root = document.getElementById("game-container")
                root.innerHTML = "";
                var header = document.createElement('h2');
                header.textContent = "Round " + (parCurrentRoundNumber + 1) + " of " + this.arrayRounds.length;

                var paragraph = document.createElement('p');
                paragraph.textContent = 'SCORE';

                var scorePlayerOne = document.createElement('p');
                scorePlayerOne.textContent = this.player1.playerName + ": " + this.player1.score;

                var scorePlayerTwo = document.createElement('p');
                scorePlayerTwo.textContent = this.player2.playerName + ": " + this.player2.score;

                var gameWinner1 = document.createElement('p');
                gameWinner1.textContent = "GAME WINNER:";
                var gameWinner2 = document.createElement('p');
                gameWinner2.textContent = rpsGame.getGameWinner();
                var gameWinner3 = document.createElement('p');
                gameWinner3.textContent = "GAME OVER!"
                var gameWinner4 = document.createElement('p');
                gameWinner4.textContent = "THANK YOU FOR PLAYING!"



                var button = document.createElement('button');
                button.classList.add('button');

                if (rpsGame.getGameWinner() === "Draw!") {
                    button.textContent = "Play Tiebreaker round";
                    button.id = 'goToRootButton';
                    button.addEventListener('click', () => {
                        const goToRoot = () => {
                            //Play the same game again!
                            rpsGame = new RPS_Game(new Player(this.player1.playerName), new Player(this.player2.playerName), numberOfRounds);
                            rpsGame.playRound(rpsGame.currentRoundNumber);
                        };
                        goToRoot();
                    });
                } else {
                    button.textContent = "NEW GAME";
                    button.id = 'goToRootButton';
                    button.addEventListener('click', () => {
                        const goToRoot = () => {
                            //go to root
                            window.location.href = '/index.html';
                        };
                        goToRoot();
                    });
                }


                var scoreBox = document.createElement('div');
                scoreBox.appendChild(scorePlayerOne);
                scoreBox.appendChild(scorePlayerTwo);
                scoreBox.classList.add('score-box');

                root.innerHTML = "";

                root.appendChild(header);
                root.appendChild(paragraph);
                root.appendChild(scoreBox);
                root.appendChild(document.createElement('br'));
                root.appendChild(document.createElement('br'));
                root.appendChild(gameWinner1);
                root.appendChild(gameWinner2);
                root.appendChild(document.createElement('br'));
                root.appendChild(document.createElement('br'));
                root.appendChild(gameWinner3);
                root.appendChild(gameWinner4);
                root.appendChild(button);

            } else {
                rpsGame.playRound(rpsGame.currentRoundNumber);
            }
        }
    }

}


/// GLOBAL VARS
var rpsGame;
var computer = false;
var playerNameOne = "";
var playerNameTwo = "";
var numberOfRounds = 0;

function importHTML(url, targetId) {
    fetch(url)
        .then(response => response.text())
        .then(html => {
            document.getElementById(targetId).innerHTML = html;
        })
        .catch(error => console.error('Error fetching HTML:', error));
}
function numberOfPlayerFunction() {
    var nop = document.querySelector('input[name="numberOfPlayer"]:checked').value;
    if (nop === "1") {
        importHTML('templates/playerInfo1.html', 'game-container');
    } else {
        importHTML('templates/playerInfo2.html', 'game-container');
    }
}
function submitPlayerInfoTwo() {
    // Get the value of the player name
    playerNameOne = document.getElementById("nameOne").value;
    playerNameTwo = document.getElementById("nameTwo").value;
    numberOfRounds = document.querySelector('input[name="rounds"]:checked').value;
    rpsGame = new RPS_Game(new Player(playerNameOne), new Player(playerNameTwo), numberOfRounds);
    rpsGame.playRound(rpsGame.currentRoundNumber);

}
function submitPlayerInfoComputer() {
    // Get the value of the player name
    playerNameOne = document.getElementById("nameOne").value;
    numberOfRounds = document.querySelector('input[name="rounds"]:checked').value;
    computer = true;
    rpsGame = new RPS_Game(new Player(playerNameOne), new Player("Computer"), numberOfRounds);
    rpsGame.playRound(rpsGame.currentRoundNumber);
}

// MAIN
importHTML('templates/numberOfPlayer.html', 'game-container');


